import { useState } from "react";
import "./Converter.css";
import { handleInputChange } from "./handleInputChange";

const Converter = () => {
  const [euro, setEuro] = useState("");
  const [dollar, setDollar] = useState("");
  const [yen, setYen] = useState("");

  const handleClear = () => {
    setEuro("");
    setDollar("");
    setYen("");
  };

  const handleSubmit = () => {
    if (euro !== "") {
      fetchConversion("EUR", euro);
    }
    if (dollar !== "") {
      fetchConversion("USD", dollar);
    }
    if (yen !== "") {
      fetchConversion("JPY", yen);
    }
  };

  const isOnlyOneInputFilled = () => {
    return [euro, dollar, yen].filter((value) => value !== "").length === 1;
  };

  const fetchConversion = async (from, amount) => {
    try {
      const response = await fetch(
        `https://v6.exchangerate-api.com/v6/c6aba20ce99035d60046ce2d/latest/${from}`
      );
      const data = await response.json();
      if (data.result === "success") {
        setEuro((data.conversion_rates["EUR"] * amount).toFixed(2));
        setYen((data.conversion_rates["JPY"] * amount).toFixed(2));
        setDollar((data.conversion_rates["USD"] * amount).toFixed(2));
      } else {
        console.error("Error fetching conversion rates:", data.error);
      }
    } catch (error) {
      console.error("Error fetching conversion rates:", error);
    }
  };

  return (
    <div className='converter'>
      <form>
        <label>Euro:</label>
        <input
          type='number'
          value={euro}
          onChange={(e) => handleInputChange(e, setEuro)}
        />
      </form>
      <form>
        <label>Dollar:</label>
        <input
          type='number'
          value={dollar}
          onChange={(e) => handleInputChange(e, setDollar)}
        />
      </form>
      <form>
        <label>Yen:</label>
        <input
          type='number'
          value={yen}
          onChange={(e) => handleInputChange(e, setYen)}
        />
      </form>
      <div className='row'>
        <button disabled={!isOnlyOneInputFilled()} onClick={handleSubmit}>
          Convert
        </button>
        <button onClick={handleClear}>Clear</button>
      </div>
    </div>
  );
};

export default Converter;
