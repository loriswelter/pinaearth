export const handleInputChange = (e, setter) => {
  const inputValue = e.target.value;

  const regex = /^\d*\.?\d{0,2}$/;
  if (regex.test(inputValue) || inputValue === "") {
    setter(inputValue);
  }
};
