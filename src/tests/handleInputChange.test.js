import { expect, test, vi } from "vitest";
import { handleInputChange } from "../handleInputChange";
test("should update the value using the setter function if the input is a valid number", () => {
  // Arrange
  const inputValue = "123.45";
  const setter = vi.fn();

  // Act
  handleInputChange({ target: { value: inputValue } }, setter);

  // Assert
  expect(setter).toHaveBeenCalledWith(inputValue);
});

test("should update the value using the setter function if the input is an empty string", () => {
  // Arrange
  const inputValue = "";
  const setter = vi.fn();

  // Act
  handleInputChange({ target: { value: inputValue } }, setter);

  // Assert
  expect(setter).toHaveBeenCalledWith(inputValue);
});

test("should not update the value if the input is not a valid number", () => {
  // Arrange
  const inputValue = "abc";
  const setter = vi.fn();

  // Act
  handleInputChange({ target: { value: inputValue } }, setter);

  // Assert
  expect(setter).not.toHaveBeenCalled();
});

test("should not update the value if the input has more than two digits after the decimal point", () => {
  // Arrange
  const inputValue = "123.456";
  const setter = vi.fn();

  // Act
  handleInputChange({ target: { value: inputValue } }, setter);

  // Assert
  expect(setter).not.toHaveBeenCalled();
});
